$(document).ready(() => {
	if($('.navbar').width() > 1000) {
		$(window).on('scroll', () => {
			if($(window).scrollTop()) {
				$('.navbar').addClass('shrink');
				$('.logo').css('width', '50px');
			} else {
				$('.navbar').removeClass('shrink');
				$('.logo').css('width', '80px');
			}
		});
	}
}); 
