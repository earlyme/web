let one = document.getElementById('one');
let two = document.getElementById('two');
let three = document.getElementById('three');
let four = document.getElementById('four');
let five = document.getElementById('five');
let six = document.getElementById('six');
let seven = document.getElementById('seven');
let eight = document.getElementById('eight');
let nine = document.getElementById('nine');
let zero = document.getElementById('zero');

let result = document.getElementById('result');

let plus = document.getElementById('plus');
let minus = document.getElementById('minus');
let multiply = document.getElementById('multiply');
let divide = document.getElementById('divide');

let equal = document.getElementById('equal');
let sqrt = document.getElementById('sqrt');
let clear = document.getElementById('clear');

let number1, number2;

let operation;

let setValue = function(val) {
	result.value += val + '';
	val = parseInt(result.value, 10);
	
	if(operation !== undefined) {
		number2 = val;
	} else {
		number1 = val;
	}
}

let domath = function() {
	let total;
	if(operation === '+')
		total = number1 + number2;
	else if(operation === '-')
		total = number1 - number2;
	else if(operation === '*')
		total = number1 * number2;
	else if(operation === '/') 		
		total = number1 / number2;
	else
		result.value = 4815162342;
	result.value = total;
	number1, number2 = undefined;
}

one.onclick = () => setValue(1);
two.onclick = () => setValue(2);
three.onclick = () => setValue(3);
four.onclick = () => setValue(4);
five.onclick = () => setValue(5);
six.onclick = () => setValue(6);
seven.onclick = () => setValue(7);
eight.onclick = () => setValue(8);
nine.onclick = () => setValue(9);
zero.onclick = () => setValue(0);

plus.onclick = () => { operation = '+'; result.value += '+'; };
minus.onclick = () => {operation = '-'; result.value += '-'; };
multiply.onclick = () => {operation = '*'; result.value += '*'; };
divide.onclick = () => {operation = '/'; result.value += '/'; };

sqrt.onclick = () => {result.value = Math.sqrt(result.value);};
equal.onclick = () => domath();

clear.onclick = () => {
	window.location.reload();
	window.location.reload(true);
};