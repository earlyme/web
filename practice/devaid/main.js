$(window).on('scroll', () => {
	if ($(window).scrollTop()) {
		$('header').addClass('bg-white');
		$('.logo-title').addClass('blue');
	} else {
		$('header').removeClass('bg-white');
		$('.logo-title').removeClass('blue');
	}
});

$('#about').on('mouseenter', () => {
	$('#nav-link-1').addClass('active');
}).on('mouseleave', () => {
	$("#nav-link-1").removeClass('active');
});