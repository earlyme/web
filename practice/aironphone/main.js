$(document).ready(() => {

  $('.header-up').addClass('sticky');
  $(window).on('scroll', () => {
    
    if($(window).scrollTop()) {
      $('.header-up').addClass('black');
      $('.logo').addClass('left-shift');
      $('ul').addClass('right-shift');
      $('a').addClass('white');
    } else {
      $('.header-up').removeClass('black');
      $('.logo').removeClass('left-shift');
      $('ul').removeClass('right-shift');
      $('a').removeClass('white');
    }
  });
});