$(document).ready(() => {
	if($('.navbar').width() > 1000) {
		$(window).on('scroll', () => {
			if($(window).scrollTop()) {
				$('.navbar').addClass('shrink');
			} else {
				$('.navbar').removeClass('shrink');
			}
		});
	}
}); 
