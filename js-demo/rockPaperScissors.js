const getUserChoice = userInput => {
  userInput = userInput.toLowerCase();
	if (userInput === 'rock' || userInput === 					'scissors' || userInput === 'paper') {
    return userInput;
  } else {
    console.log('Error: Invalid input.');
  }
}


function getComputerChoice() {
  var number = Math.floor(Math.random() * 3);
  
  switch(number) {
    case 0: return 'rock';
    case 1: return 'paper';
    case 2: return 'scissors';
    default: return 'Error: invalid number';
  }
}
let computerChoice = getComputerChoice( );

function determineWinner(userChoice, 					computerChoice) {
	if(userChoice === computerChoice) {
		return 'Draw!';
  }
  else {
    if(userChoice === 'rock') {
      if(computerChoice === 'paper') {
        return 'Computer won!';
      } else {
        return 'User won!';
      }
  	}
    
    if(userChoice === 'paper') {
      if(computerChoice === 'rock') {
        return 'User won!';
      } else {
        return 'Computer won!';
      }
  	}
    
    if(userChoice === 'scissors') {
      if(computerChoice === 'paper') {
        return 'User won!';
      } else {
        return 'Computer won!';
      }
  	}
  }
}

function playGame( ) {
  let userChoice = getUserChoice('scissors');
  let computerChoice = getComputerChoice();
  
  console.log(`Computer choice: ${computerChoice}`);
  console.log(`User choice: ${userChoice}`);
  
  console.log(determineWinner(userChoice, computerChoice));
}

playGame();