$(document).ready(() => {

    $('.nav-link').on('mouseenter', event => {
        $(event.currentTarget).addClass('red');
    })
    .on('mouseleave', event => {
        $(event.currentTarget).removeClass('red');
    });

    $('.nav-link').on('click', () => {
        $('.nav-link').css({
            textTransform: 'capitalize' 
        });
    });

    $('#services-title').on('click', () => {
        $('#services-blog-1').toggleClass('hide');
    });

    $(window).on('scroll', () => {
        if($(window).scrollTop()) {
            $('.navbar').addClass('stuck');
        } else {
            $('.navbar').removeClass('stuck');
        }
    });

    $('#btn-arrow').on('mouseenter', () => {
        $('#btn-arrow').addClass('red');
    }).on('mouseleave', () => {
        $('#btn-arrow').removeClass('red');
    });
});