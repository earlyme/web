import React from 'react';

const divStyle = {
  width: '500px',
  height: '200px',
  textAlign: 'center',
  border: '3px solid pink',
  margin: '0 auto'
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {userInput: ''};
  	this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({
      userInput: e.target.value
    });
  }

  render() {
    return (
      <div style={divStyle}>
        <input type="text" onChange={this.handleChange} value={this.state.userInput}/>
        <h1>{this.state.userInput}</h1>
      </div>
    );
  }
}

export default App;
