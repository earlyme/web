import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Flashy from './Flashy';

ReactDOM.render(<Flashy color='red'/>, document.getElementById('root'));

setTimeout(() => {
    ReactDOM.render(
      <Flashy color='green'/>,
      document.getElementById('root')
    );
  }, 2000);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
