$(document).ready(() => {
	$('.menu').on('mouseenter', () => {
    $('.nav-menu').show();
  });
  $('.nav-menu').on('mouseleave', () => {
    $('.nav-menu').hide();
  });
  
  $('.btn').on('mouseenter', event => {
    $(event.currentTarget).addClass('btn-hover');
  }).on('mouseleave', event => {
    $(event.currentTarget).removeClass('btn-hover');
  });
 
  $('.postText').on('keyup', (event) => {
    
    const post = $(event.currentTarget).val();
    const remaining = 50 - post.length;
    $('.characters').html(remaining);
    
    if(remaining <= 0) {
      $('.wordcount').addClass('red');
      $('.wordcount').addClass('bold');
    } else {
      $('.wordcount').removeClass('red');
      $('.wordcount').removeClass('bold');
    }
  });
  
  $('postText').focus();

  $('.avatar').on('click', event => {
    $(event.currentTarget).fadeOut('slow');
  });

  $('.btn').on('click', event => {
    $('.avatar').fadeIn('slow');
  });
}); 
